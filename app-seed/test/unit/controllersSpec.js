'use strict';

/* jasmine specs for controllers go here */

describe('controllers', function(){
    var scope, MyCtrl1;

    beforeEach(module('myApp.controllers'));

    describe('MyCtrl1', function () {

        beforeEach(inject(function ($rootScope, $controller) {
            scope = $rootScope.$new();

            $controller('MyCtrl1', {
                $scope: scope
            });

            spyOn(scope, 'init').andCallThrough();
            scope.init();
            scope.$digest();
        }));

        it('should run init', inject(function() {
            expect(scope.init).toHaveBeenCalled();
            expect(scope.test).toBeDefined();
            expect(scope.test).toEqual('Hello World!');
        }));
    });
});
