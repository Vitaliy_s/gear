'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

describe('my app', function() {

  beforeEach(function() {
    browser().navigateTo('../../app/index.html');
  });


  it('should automatically redirect to /home when location hash/fragment is empty', function() {
    expect(browser().location().url()).toBe("/home");
  });


  describe('contacts', function() {

    beforeEach(function() {
      browser().navigateTo('#/contacts');
    });


    it('should render contacts when user navigates to /contacts', function() {
      expect(element('[data-ng-view] h1:first').text()).toMatch('Contacts');
    });

  });


  describe('home', function() {

    beforeEach(function() {
      browser().navigateTo('#/home');
    });


    it('should render home when user navigates to /home', function() {
      expect(element('[data-ng-view] h1:first').text()).toMatch('Vendim');
    });

  });
});
