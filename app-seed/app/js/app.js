'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives', 'myApp.controllers']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/contacts', {templateUrl: 'partials/contacts.html', controller: 'contacts'});
    $routeProvider.when('/home', {templateUrl: 'partials/home.html', controller: 'home'});
    $routeProvider.otherwise({redirectTo: '/home'});
  }]);
  //TODO: rename all instance of myApp
