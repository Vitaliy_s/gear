'use strict';

/* Controllers */

angular.module('myApp.controllers', [])

.controller('MyCtrl1', function ($scope) {
    $scope.test = "";

    $scope.init = function () {
        $scope.test = "Hello World!";
    };
})

.controller('contacts', function ($scope, $rootScope) {

    //console.log(angular.element('.contacts-form'));

})

.controller('home',function(){
    return false;
});
