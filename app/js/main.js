/**
 * Require js configuration thanks cap
 */

require.config({
    paths: {
        'domReady': './libs/require-jquery/domReady',
        'twitter-bootstrap': './libs/twitter-bootstrap/bootstrap',
        'holder': './libs/twitter-bootstrap/holder',
        'underscore': './libs/underscore/underscore',
        'angular': './libs/angular/1.1.4/angular',
        'angular-bootstrap': './libs/angular/1.1.4/angular-bootstart',
        'angular-cookies': './libs/angular/1.1.4/angular-cookies',
        'angular-sanitize': './libs/angular/1.1.4/angular-sanitize',
        'angular-resources': './libs/angular/1.1.4/angular-resources',
        'angular-mocks': './libs/angular/1.1.4/angular-mocks'
    },
    shim: {
        'angular': {
            exports: 'angular',
            deps: ['underscore', 'twitter-bootstrap']
        },
        'angular-bootstrap': {
            deps: ['angular']
        },
        'angular-cookies': {
            deps: ['angular']
        },
        'angular-sanitize': {
            deps: ['angular']
        },
        'angular-resourcess': {
            deps: ['angular']
        },
        'angular-mocks': {
            deps: ['angular']
        }
    }
});

require(['./bootstrap']);
