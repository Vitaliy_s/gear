/**
 * Routes of dev gears application
 */

define(['./app'], function (app) {

    app.config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/', {
            templateUrl: 'partials/home.html',
            controller: 'home'
        });

        $routeProvider.when('/contacts', {
            templateUrl: 'partials/contacts.html',
            controller: 'contacts'
        });

        $routeProvider.otherwise({
            redirectTo: '/'
        });

    }]);

});
