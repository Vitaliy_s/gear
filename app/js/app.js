/**
 * devGears Application
 */

define([
    'angular',
    './controllers/index',
    './controllers/home-ctrl',
    './controllers/contacts-ctrl'
], function (ng) {
    'use strict';

    return ng.module('devGearsApp', [
        'devGearsApp.controllers'
    ]);
});
