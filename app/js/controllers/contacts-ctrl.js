/**
 * Contacts controller
 */

define(['./index'], function (controllers) {
    'use strict';

    controllers.controller('contacts', function ($scope, $rootScope) {
        var form = $('.contacts-form');
        form.on('submit', function () {
            var serializedForm = form.serialize();
            $.ajax({
                data: serializedForm,
                type: form.attr('method'),
                url: form.attr('action'),
                success: function () {
                }
            });
            return false;
        });
    });

});
