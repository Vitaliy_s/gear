/**
 * Main controller's module
 */

define(['angular', '../app'], function (ng) {
    'use strict';

    return ng.module('devGearsApp.controllers', []);
});
