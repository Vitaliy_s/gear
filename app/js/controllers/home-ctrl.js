/**
 * Home controller
 */

define(['./index', 'holder'], function (controllers, holder) {
    'use strict';

    controllers.controller('home', function () {
        holder.run();
    });

});
