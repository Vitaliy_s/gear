/**
 * Bootstrap (Drop) point of dev gears application
 */

require([
    'angular',
    './app',
    './routes',
    'services/pending-services'
], function (ng, app, routes, pendingServices) {
    app.run(pendingServices.init);

    require(['domReady!'], function (document) {
        return ng.bootstrap(document, ['devGearsApp']);
    });
});
